package com.atguigu.jxc;


import com.atguigu.jxc.entity.HomePage;
import com.atguigu.jxc.service.HomePageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Map;

@SpringBootTest
public class JxcApplicationTests {

	@Autowired
	private HomePageService homePageService;
	@Test
	public void contextLoads() {
		HomePage homePage = new HomePage();
		homePage.setPage(1);
		homePage.setRows(30);
		Map<String, Object> map = homePageService.listInventory(homePage);
		System.out.println(map);
	}

}
