package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.SupplierListVo;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-11:34
 */
@RestController
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     * @param supplierListVo
     * @return
     */
    @PostMapping("/supplier/list")
    public Map<String,Object> supplierList(SupplierListVo supplierListVo) {


        return supplierService.supplierList(supplierListVo);
    }

    /**
     * 添加或修改供应商
     * @param supplier
     * @return
     */
    @PostMapping("supplier/save")
    public ServiceVO supplierSaveUpdate(Supplier supplier) {

        supplierService.supplierSaveUpdate(supplier);
        return ServiceVO.ok();
    }

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    @PostMapping("/supplier/delete")
    public ServiceVO deleteSupplier(String ids) {

        supplierService.deleteSupplier(ids);
        return ServiceVO.ok();
    }
}
