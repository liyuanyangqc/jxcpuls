package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.HomePage;
import com.atguigu.jxc.service.HomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 *
 *
 * 首页
 * @author shkstart
 * @create 2023-12-19-8:57
 */
@RestController
public class HomePageController {

    @Autowired
    private HomePageService homePageService;

    @PostMapping("/goods/listInventory")
    public Map<String,Object> listInventory(HomePage homePage) {

        Map<String,Object> resultMap = homePageService.listInventory(homePage);
        return resultMap;
    }
}
