package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-20-9:35
 */
@RestController
public class DamageGoodsController {

    @Autowired
    private DamageGoodsService damageGoodsService;

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> damageListGoods(Integer damageListId) {

        return damageGoodsService.damageListGoods(damageListId);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/damageListGoods/list")
    public Map<String,Object> damageListGoodsList(String sTime, String eTime) {

        return damageGoodsService.damageListGoodsList(sTime,eTime);
    }

    /**
     * 添加报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/damageListGoods/save")
    public ServiceVO saveDamageGoods(DamageList damageList, String damageListGoodsStr, HttpSession session) {

        damageGoodsService.saveDamageGoods(damageList,damageListGoodsStr,session);
        return ServiceVO.ok();
    }
}
