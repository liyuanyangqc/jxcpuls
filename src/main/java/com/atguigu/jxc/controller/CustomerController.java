package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.CustomerListVo;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-14:42
 */
@RestController
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    /**
     * 分页查询客户
     * @param customerListVo
     * @return
     */
    @PostMapping("/customer/list")
    public Map<String,Object> supplierList(CustomerListVo customerListVo) {

        return customerService.customerList(customerListVo);
    }

    /**
     * 添加或修改客户
     * @param customer
     * @return
     */
    @PostMapping("/customer/save")
    public ServiceVO customerSaveUpdate(Customer customer) {

        customerService.customerSaveUpdate(customer);
        return ServiceVO.ok();
    }

    /**
     * 删除客户
     * @param ids
     * @return
     */
    @PostMapping("/customer/delete")
    public ServiceVO deleteCustomer(String ids) {

        customerService.deleteCustomer(ids);
        return ServiceVO.ok();
    }
}
