package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.DamageGoodsService;
import com.atguigu.jxc.service.OverFlowGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-20-9:35
 */
@RestController
public class OverFlowGoodsController {

    @Autowired
    private OverFlowGoodsService overFlowGoodsService;

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> overflowListGoods(Integer overflowListId) {

        return overFlowGoodsService.overflowListGoods(overflowListId);
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> overflowListGoodsList(String sTime, String eTime) {

        return overFlowGoodsService.overflowListGoodsList(sTime,eTime);
    }

    /**
     * 添加报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @param session
     * @return
     */
    @PostMapping("/overflowListGoods/save")
    public ServiceVO saveOverFlowGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {

        overFlowGoodsService.saveOverFlowGoods(overflowList,overflowListGoodsStr,session);
        return ServiceVO.ok();
    }
}
