package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-19-15:06
 */
public interface CustomerDao {

    /**
     * 分页查询客户
     * @param customerPage
     * @param customerName
     * @return
     */
    List<Customer> customerList(Page<Customer> customerPage, @Param("customerName") String customerName);

    /**
     * 添加客户
     * @param customer
     */
    void saveCustomer(Customer customer);

    /**
     * 修改客户
     * @param customer
     */
    void updateCustomer(Customer customer);

    /**
     * 删除客户
     * @param customerId
     */
    void deleteCustomer(int customerId);
}
