package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.HomePage;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-19-9:29
 */
@Mapper
public interface HomePageDao {


    List<Goods> listInventory(Page<Goods> page, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);
}
