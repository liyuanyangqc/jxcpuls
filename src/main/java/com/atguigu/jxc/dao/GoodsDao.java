package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    /**
     * 分页查询商品信息
     * @param goodsPage
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    List<Goods> goodsList(Page<Goods> goodsPage, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 获取二级分类下的商品
     * @param goodsPage
     * @param goodsName
     * @param collect
     * @return
     */
    List<Goods> goodsPidList(Page<Goods> goodsPage, @Param("goodsName") String goodsName, @Param("collect") List<Integer> collect);

    /**
     * 添加商品信息
     * @param goods
     */
    void saveGoods(Goods goods);

    /**
     * 修改商品信息
     * @param goods
     */
    void updateGoods(Goods goods);

    /**
     * 删除商品信息
     * @param goodsId
     */
    void deleteGoods(Integer goodsId);

    /**
     * 根据id获取商品信息
     * @param goodsId
     * @return
     */
    Goods getGoodsById(Integer goodsId);

    /**
     * 分页查询无库存商品信息
     * @param goodsPage
     * @param nameOrCode
     * @return
     */
    List<Goods> getNoInventoryQuantity(Page<Goods> goodsPage, @Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(Page<Goods> goodsPage, @Param("nameOrCode") String nameOrCode);

    /**
     * 删除商品库存
     * @param goodsId
     */
    void deleteStock(Integer goodsId);

    /**
     * 查询库存报警商品信息
     * @return
     */
    List<Goods> listAlarmGoods();

}
