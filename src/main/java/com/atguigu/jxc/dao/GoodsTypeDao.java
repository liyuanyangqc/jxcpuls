package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    /**
     * 根据id获取信息
     * @param goodsTypeId
     * @return
     */
    GoodsType getGoodsTypeById(Integer goodsTypeId);

    /**
     * 获取所有分类的叶子节点
     * @param goodsTypeId
     * @return
     */
    List<GoodsType> getParentIdList(Integer goodsTypeId);

    /**
     * 新增分类
     * @param goodsType
     */
    void saveGoodsType(GoodsType goodsType);


    /**
     * 删除分类
     * @param goodsTypeId
     */
    void deleteGoodsType(Integer goodsTypeId);
}
