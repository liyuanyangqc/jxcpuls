package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-20-9:38
 */
public interface DamageGoodsDao {

    /**
     * 添加商品报损单商品列表
     * @param listGoods
     */
    void saveDamageGoods(@Param("listGoods") List<DamageListGoods> listGoods);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    List<DamageListGoods> selectDamageGoodsById(Integer damageListId);
}
