package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-20-9:45
 */
public interface DamageDao {

    /**
     * 添加商品报损单
     * @param damageList
     */
    void saveDamage(DamageList damageList);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> selectDamageGoods(@Param("sTime") String sTime, @Param("eTime") String eTime);


}
