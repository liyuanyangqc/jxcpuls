package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-19-15:52
 */
public interface UnitDao {

    /**
     * 查询所有商品单位
     * @return
     */
    List<Unit> UnitList();
}
