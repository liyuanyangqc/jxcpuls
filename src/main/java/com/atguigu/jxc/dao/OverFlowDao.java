package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-20-9:45
 */
public interface OverFlowDao {

    /**
     * 添加商品报损单
     * @param overflowList
     */
    void saveOverFlow(OverflowList overflowList);

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    List<OverflowList> selectDamageGoods(@Param("sTime") String sTime, @Param("eTime") String eTime);
}
