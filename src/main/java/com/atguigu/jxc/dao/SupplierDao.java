package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-19-13:49
 */
public interface SupplierDao {

    /**
     * 分页查询供应商
     * @param supplierPage
     * @param supplierName
     * @return
     */
    List<Supplier> supplierList(Page<Supplier> supplierPage, @Param("supplierName") String supplierName);

    /**
     * 添加供应商
     * @param supplier
     */
    void saveSupplier(Supplier supplier);

    /**
     * 修改供应商
     * @param supplier
     */
    void updateSupplier(Supplier supplier);

    /**
     * 删除供应商
     * @param supplierId
     */
    void deleteSupplier(int supplierId);
}
