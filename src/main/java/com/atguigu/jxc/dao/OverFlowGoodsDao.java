package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shkstart
 * @create 2023-12-20-9:38
 */
public interface OverFlowGoodsDao {

    /**
     * 添加商品报损单商品列表
     * @param listGoods
     */
    void saveOverFlowGoods(@Param("listGoods") List<OverflowListGoods> listGoods);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    List<OverflowListGoods> selectDamageGoodsById(Integer overflowListId);
}
