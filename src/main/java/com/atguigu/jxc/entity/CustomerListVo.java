package com.atguigu.jxc.entity;

import lombok.Data;

/**
 * @author shkstart
 * @create 2023-12-19-15:28
 */
@Data
public class CustomerListVo {

    private Integer page;
    private Integer rows;
    private String customerName;
}
