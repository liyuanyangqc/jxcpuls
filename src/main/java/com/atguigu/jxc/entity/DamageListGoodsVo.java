package com.atguigu.jxc.entity;

import lombok.Data;

/**
 * @author shkstart
 * @create 2023-12-20-11:23
 */
@Data
public class DamageListGoodsVo {

    private Integer damageListGoodsId;
    private Integer goodsId;
    private String goodsCode;
    private String goodsName;
    private String goodsModel;
    private String goodsUnit;
    private Integer goodsNum;
    private double price;
    private double total;
    private Integer damageListId;
    private Integer goodsTypeId;

    private Integer lastPurchasingPrice;
}
