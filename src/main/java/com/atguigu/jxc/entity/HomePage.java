package com.atguigu.jxc.entity;

import lombok.Data;

/**
 * @author shkstart
 * @create 2023-12-19-9:03
 */
@Data
public class HomePage {

    private Integer page;
    private Integer rows;
    private String codeOrName;
    private Integer goodsTypeId;
}
