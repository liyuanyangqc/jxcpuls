package com.atguigu.jxc.entity;

import lombok.Data;

/**
 * @author shkstart
 * @create 2023-12-19-11:51
 */
@Data
public class SupplierListVo {

    private Integer page;
    private Integer rows;
    private String supplierName;
}
