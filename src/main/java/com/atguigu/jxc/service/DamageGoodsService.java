package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-20-9:38
 */
public interface DamageGoodsService{

    /**
     * 添加报损单
     * @param damageList
     * @param damageListGoodsStr
     */
    void saveDamageGoods(DamageList damageList, String damageListGoodsStr, HttpSession session);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> damageListGoodsList(String sTime, String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    Map<String, Object> damageListGoods(Integer damageListId);
}
