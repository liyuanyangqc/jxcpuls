package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-20-9:38
 */
public interface OverFlowGoodsService {

    /**
     * 添加报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     */
    void saveOverFlowGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session);

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> overflowListGoodsList(String sTime, String eTime);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> overflowListGoods(Integer overflowListId);
}
