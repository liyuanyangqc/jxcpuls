package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.SupplierListVo;

import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-11:53
 */
public interface SupplierService {

    /**
     * 分页查询供应商
     * @param supplierListVo
     * @return
     */
    Map<String, Object> supplierList(SupplierListVo supplierListVo);

    /**
     * 添加或修改供应商
     * @param supplier
     */
    void supplierSaveUpdate(Supplier supplier);

    /**
     * 删除供应商
     * @param ids
     */
    void deleteSupplier(String ids);
}
