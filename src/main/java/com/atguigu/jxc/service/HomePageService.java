package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.HomePage;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-9:00
 */

public interface HomePageService {
    Map<String, Object> listInventory(HomePage homePage);
}
