package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.CustomerListVo;
import com.atguigu.jxc.entity.SupplierListVo;

import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-14:50
 */
public interface CustomerService {

    /**
     * 分页查询客户
     * @param customerListVo
     * @return
     */
    Map<String, Object> customerList(CustomerListVo customerListVo);

    /**
     * 添加或修改客户
     * @param customer
     */
    void customerSaveUpdate(Customer customer);

    /**
     * 删除客户
     * @param ids
     */
    void deleteCustomer(String ids);
}
