package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.HomePageDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.HomePage;
import com.atguigu.jxc.service.HomePageService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-9:00
 */
@Service
public class HomePageServiceImpl implements HomePageService {


    @Autowired
    private HomePageDao homePageDao;

    @Override
    public Map<String, Object> listInventory(HomePage homePage) {


        Page<Goods> page = new Page<>(homePage.getPage(), homePage.getRows());
        List<Goods> list = homePageDao.listInventory(page,homePage.getCodeOrName(),homePage.getGoodsTypeId());

        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }
}
