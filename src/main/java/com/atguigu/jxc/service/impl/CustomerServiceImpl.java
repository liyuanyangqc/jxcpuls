package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.CustomerListVo;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.SupplierListVo;
import com.atguigu.jxc.service.CustomerService;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-14:50
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 分页查询客户
     * @param customerListVo
     * @return
     */
    @Override
    public Map<String, Object> customerList(CustomerListVo customerListVo) {

        Page<Customer> customerPage = new Page<>(customerListVo.getPage(), customerListVo.getRows());
        List<Customer> list = customerDao.customerList(customerPage,customerListVo.getCustomerName());
        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }

    /**
     * 添加或修改客户
     * @param customer
     */
    @Override
    public void customerSaveUpdate(Customer customer) {

        if (customer.getCustomerId() == null) {
            customerDao.saveCustomer(customer);
        } else {
            customerDao.updateCustomer(customer);
        }
    }

    /**
     * 删除客户
     * @param ids
     */
    @Override
    public void deleteCustomer(String ids) {

        String[] split = ids.split(",");
        for (String supplierId : split) {
            customerDao.deleteCustomer(Integer.parseInt(supplierId));
        }

    }
}
