package com.atguigu.jxc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.DamageGoodsDao;
import com.atguigu.jxc.dao.OverFlowDao;
import com.atguigu.jxc.dao.OverFlowGoodsDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageGoodsService;
import com.atguigu.jxc.service.OverFlowGoodsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author shkstart
 * @create 2023-12-20-9:38
 */
@Service
public class OverFlowGoodsServiceImpl implements OverFlowGoodsService {

    @Autowired
    private OverFlowDao overFlowDao;

    @Autowired
    private OverFlowGoodsDao overFlowGoodsDao;

    /**
     * 添加报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @param session
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOverFlowGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {

        User user = (User) session.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        overFlowDao.saveOverFlow(overflowList);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            OverflowListGoodsVo[] overflowListGoodsVos = objectMapper.readValue(overflowListGoodsStr, OverflowListGoodsVo[].class);
            List<OverflowListGoodsVo> listGoodsVo = Arrays.asList(overflowListGoodsVos);
            List<OverflowListGoods> listGoods = listGoodsVo.stream().map(overflowListGoodsVo -> {
                overflowListGoodsVo.setOverflowListId(overflowList.getOverflowListId());
                return BeanUtil.copyProperties(overflowListGoodsVo, OverflowListGoods.class);
            }).collect(Collectors.toList());
            overFlowGoodsDao.saveOverFlowGoods(listGoods);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> overflowListGoodsList(String sTime, String eTime) {

        List<OverflowList> list = overFlowDao.selectDamageGoods(sTime,eTime);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> overflowListGoods(Integer overflowListId) {

        List<OverflowListGoods> list = overFlowGoodsDao.selectDamageGoodsById(overflowListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
