package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.SupplierListVo;
import com.atguigu.jxc.service.SupplierService;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-11:53
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    /**
     * 分页查询供应商
     * @param supplierListVo
     * @return
     */
    @Override
    public Map<String, Object> supplierList(SupplierListVo supplierListVo) {

        Page<Supplier> supplierPage = new Page<>(supplierListVo.getPage(), supplierListVo.getRows());
        List<Supplier> list = supplierDao.supplierList(supplierPage,supplierListVo.getSupplierName());
        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }

    @Override
    public void supplierSaveUpdate(Supplier supplier) {

        if (supplier.getSupplierId() == null) {
            supplierDao.saveSupplier(supplier);
        } else {
            supplierDao.updateSupplier(supplier);
        }
    }

    /**
     * 删除供应商
     * @param ids
     */
    @Override
    public void deleteSupplier(String ids) {

        String[] split = ids.split(",");
        for (String supplierId : split) {
            supplierDao.deleteSupplier(Integer.parseInt(supplierId));
        }

    }
}
