package com.atguigu.jxc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.DamageGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.DamageListGoodsVo;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageGoodsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author shkstart
 * @create 2023-12-20-9:38
 */
@Service
public class DamageGoodsServiceImpl implements DamageGoodsService {

    @Autowired
    private DamageGoodsDao damageGoodsDao;

    @Autowired
    private DamageDao damageDao;

    /**
     * 添加报损单
     * @param damageList
     * @param damageListGoodsStr
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveDamageGoods(DamageList damageList, String damageListGoodsStr, HttpSession session) {

        User user = (User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        damageDao.saveDamage(damageList);
        System.out.println(damageList.getDamageListId());
        DamageListGoodsVo[] damageListGoodsVoList = JSON.parseObject(damageListGoodsStr, DamageListGoodsVo[].class);

        List<DamageListGoodsVo> listGoodsVo = Arrays.asList(damageListGoodsVoList);
        List<DamageListGoods> listGoods = listGoodsVo.stream().map(damageListGoodsVo -> {
            damageListGoodsVo.setDamageListId(damageList.getDamageListId());
            return BeanUtil.copyProperties(damageListGoodsVo, DamageListGoods.class);
        }).collect(Collectors.toList());
        damageGoodsDao.saveDamageGoods(listGoods);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> damageListGoodsList(String sTime, String eTime) {

        List<DamageList> list = damageDao.selectDamageGoods(sTime,eTime);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> damageListGoods(Integer damageListId) {

        List<DamageListGoods> list = damageGoodsDao.selectDamageGoodsById(damageListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
