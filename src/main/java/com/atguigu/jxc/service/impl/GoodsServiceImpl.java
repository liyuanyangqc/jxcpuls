package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.*;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品信息
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        Map<String, Object> map = new HashMap<>();
        if (goodsTypeId != null) {
            GoodsType goodsType = goodsTypeDao.getGoodsTypeById(goodsTypeId);
            if (goodsType.getGoodsTypeState() == 0) {
                Page<Goods> goodsPage = new Page<>(page, rows);
                List<Goods> list = goodsDao.goodsList(goodsPage,goodsName,goodsTypeId);
                map.put("total",list.size());
                map.put("rows",list);
            } else {
                if (goodsType.getPId() == -1) {
                    Page<Goods> goodsPage = new Page<>(page, rows);
                    List<Goods> list = goodsDao.goodsList(goodsPage,goodsName,null);
                    map.put("total",list.size());
                    map.put("rows",list);
                } else {
                    List<GoodsType> goodsTypeList = goodsTypeDao.getParentIdList(goodsType.getGoodsTypeId());
                    Page<Goods> goodsPage = new Page<>(page, rows);

                    List<Integer> collect = goodsTypeList.stream().map(GoodsType::getGoodsTypeId).collect(Collectors.toList());
                    List<Goods> list = goodsDao.goodsPidList(goodsPage,goodsName,collect);

                    map.put("total",list.size());
                    map.put("rows",list);
                }
            }
        } else {
            Page<Goods> goodsPage = new Page<>(page, rows);
            List<Goods> list = goodsDao.goodsList(goodsPage,goodsName,goodsTypeId);
            map.put("total",list.size());
            map.put("rows",list);
        }

        return map;
    }

    /**
     * 添加或修改商品信息
     * @param goods
     */
    @Override
    public void saveGoods(Goods goods) {

        if (goods.getGoodsId() == null) {
            String code = goodsDao.getMaxCode();
            String[] codes = code.split("");
            int index = 0;
            for (int i = 0; i < codes.length; i++) {
                if (Integer.parseInt(codes[i]) > 0) {
                    index = i;
                    break;
                }
            }
            goods.setGoodsCode(code.substring(0,index) + (Integer.parseInt(code) + 1));
            goodsDao.saveGoods(goods);
        } else {
            goodsDao.updateGoods(goods);
        }
    }

    /**
     * 删除商品信息
     * @param goodsId
     */
    @Override
    public void deleteGoods(Integer goodsId) {

        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() != 0) {
            throw new RuntimeException();
        }
        goodsDao.deleteGoods(goodsId);
    }

    /**
     * 分页查询无库存商品信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {

        Page<Goods> goodsPage = new Page<>(page, rows);
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(goodsPage,nameOrCode);
        Map<String, Object> map = new HashMap<>();
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {

        Page<Goods> goodsPage = new Page<>(page, rows);
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(goodsPage,nameOrCode);
        Map<String, Object> map = new HashMap<>();
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    /**
     * 添加商品期初库存
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {

        Goods goods = new Goods();
        if (goodsId != null && goodsId > 0) {
            if (inventoryQuantity < 0) {
                throw new RuntimeException();
            }
            goods.setGoodsId(goodsId);
            goods.setInventoryQuantity(inventoryQuantity);
            goods.setPurchasingPrice(purchasingPrice);
        }
        goodsDao.updateGoods(goods);
    }

    /**
     * 删除商品库存
     * @param goodsId
     */
    @Override
    public void deleteStock(Integer goodsId) {

        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() != 0) {
            throw new RuntimeException();
        }
        goodsDao.deleteStock(goodsId);
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @Override
    public Map<String, Object> listAlarmGoods() {

        List<Goods> goodsList = goodsDao.listAlarmGoods();
        Map<String, Object> map = new HashMap<>();
        map.put("rows",goodsList);
        return map;
    }


}
