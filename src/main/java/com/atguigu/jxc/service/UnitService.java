package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @author shkstart
 * @create 2023-12-19-15:51
 */
public interface UnitService {

    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, Object> unitList();
}
