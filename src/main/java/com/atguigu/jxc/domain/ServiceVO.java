package com.atguigu.jxc.domain;

import com.azul.crs.client.Result;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 后端返回的实体
 */
@Data
public class ServiceVO<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;
    private T info;

    public ServiceVO() {}

    public ServiceVO(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ServiceVO(int code, String msg, T info) {
        this.code = code;
        this.msg = msg;
        this.info = info;
    }

    protected static <T> ServiceVO<T> build(T data) {
        ServiceVO<T> serviceVO = new ServiceVO<>();
        if (data != null)
            serviceVO.setInfo(data);
        return serviceVO;
    }

    public static<T> ServiceVO<T> ok(T data){
        ServiceVO<T> serviceVO = build(data);
        serviceVO.setCode(100);
        serviceVO.setMsg("成功");
        return serviceVO;
    }

    public static<T> ServiceVO<T> ok(){
        return ServiceVO.ok(null);
    }



}
